import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'rick-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  isScrolling: boolean;
  @HostListener('window:scroll', ['$event'])
  onScroll(): void {
    this.isScrolling = window.scrollY > 0;
  }
}
