import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Result } from 'src/app/interfaces/character-dto';

@Component({
  selector: 'one-character-card',
  templateUrl: './one-character-card.component.html',
  styleUrls: ['./one-character-card.component.css'],
})
export class OneCharacterCardComponent implements OnInit {
  @Input() oneCharacter$: Observable<Result>;

  constructor() {}

  ngOnInit(): void {}
}
