import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CharacterDTO } from 'src/app/interfaces/character-dto';

@Component({
  selector: 'character-card',
  templateUrl: './character-card.component.html',
  styleUrls: ['./character-card.component.css'],
})
export class CharacterCardComponent implements OnInit {
  @Input() character: Observable<CharacterDTO>;

  constructor() {}

  ngOnInit(): void {}
}
