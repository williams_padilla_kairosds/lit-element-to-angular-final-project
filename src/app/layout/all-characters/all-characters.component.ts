import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { CharacterDTO } from 'src/app/interfaces/character-dto';
import { CharacterProxyService } from '../../services/proxy/character-proxy.service';

@Component({
  selector: 'characters-page',
  templateUrl: './all-characters.component.html',
  styleUrls: ['./all-characters.component.css'],
})
export class AllCharactersComponent implements OnInit {
  characters$: Observable<CharacterDTO>;
  page: number;
  form: FormGroup;
  allStatus: Array<string>;

  constructor(private service: CharacterProxyService) {}

  ngOnInit(): void {
    this.characters$ = this.service.getCharacters();
    this.page = 1;
    this.form = new FormGroup({
      name: new FormControl(''),
      status: new FormControl(''),
    });
    this.allStatus = ['Alive', 'Dead', 'Unknown'];
  }

  nextCharacters(): void {
    this.page++;
    this.characters$ = this.service.getMoreCharacters(this.page);
  }

  prevCharacters(): void {
    this.page--;
    this.characters$ = this.service.getMoreCharacters(this.page);
  }

  searchCharacter(): void {
    const name = this.form.value.name;
    const status = this.form.value.status;
    this.characters$ = this.service.getFilteredCharacter(name, status);
  }
}
