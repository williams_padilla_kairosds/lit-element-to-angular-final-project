import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CharacterCardComponent } from '../components/character-card/character-card.component';
import { OneCharacterCardComponent } from '../components/one-character-card/one-character-card.component';
import { AllCharactersComponent } from './all-characters/all-characters.component';
import { HomeComponent } from './home/home.component';
import { RandomCharacterComponent } from './random-character/random-character.component';

@NgModule({
  declarations: [
    HomeComponent,
    AllCharactersComponent,
    RandomCharacterComponent,
    CharacterCardComponent,
    OneCharacterCardComponent,
  ],
  imports: [CommonModule, ReactiveFormsModule],
})
export class LayoutModule {}
