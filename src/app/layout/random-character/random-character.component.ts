import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CharacterDTO, Result } from 'src/app/interfaces/character-dto';
import { CharacterProxyService } from 'src/app/services/proxy/character-proxy.service';
import { CharactersService } from 'src/app/services/proxy/characters.service';

@Component({
  selector: 'random-character',
  templateUrl: './random-character.component.html',
  styleUrls: ['./random-character.component.css'],
})
export class RandomCharacterComponent implements OnInit {
  singleCharacter$: Observable<Result>;
  firstCharacter: number;
  characterInfo$: Observable<CharacterDTO>;

  constructor(
    private service: CharactersService,
    private info: CharacterProxyService
  ) {}

  ngOnInit(): void {
    this.firstCharacter = 1;
    this.singleCharacter$ = this.info.getASingleCharacter(this.firstCharacter);
    this.characterInfo$ = this.info.getCharacters();
  }

  random(id: number): void {
    this.singleCharacter$ = this.service.randomCharacter(id);
  }
}
