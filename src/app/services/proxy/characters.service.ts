import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Result } from 'src/app/interfaces/character-dto';
import { CharacterProxyService } from './character-proxy.service';

@Injectable({
  providedIn: 'root',
})
export class CharactersService {
  constructor(private repository: CharacterProxyService) {}

  randomCharacter(id: number): Observable<Result> {
    let randomChar = Math.floor(Math.random() * id);
    return this.repository.getASingleCharacter(randomChar).pipe();
  }
}
