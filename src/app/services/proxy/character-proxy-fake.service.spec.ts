import { TestBed } from '@angular/core/testing';

import { CharacterProxyFakeService } from './character-proxy-fake.service';

describe('CharacterProxyFakeService', () => {
  let service: CharacterProxyFakeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CharacterProxyFakeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
