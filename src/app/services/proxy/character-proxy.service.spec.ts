import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { CharacterProxyService } from './character-proxy.service';

describe('CharacterProxyService', () => {
  let service: CharacterProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
    service = TestBed.inject(CharacterProxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
