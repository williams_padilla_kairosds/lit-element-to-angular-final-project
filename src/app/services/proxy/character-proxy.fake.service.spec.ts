import { TestBed } from '@angular/core/testing';

import { CharacterProxy.FakeService } from './character-proxy.fake.service';

describe('CharacterProxy.FakeService', () => {
  let service: CharacterProxy.FakeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CharacterProxy.FakeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
