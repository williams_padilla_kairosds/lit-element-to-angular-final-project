import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CharacterDTO, Result } from 'src/app/interfaces/character-dto';

@Injectable({
  providedIn: 'root',
})
export class CharacterProxyService {
  api: string = 'https://rickandmortyapi.com/api/character';

  constructor(private httpService: HttpClient) {}

  getCharacters(): Observable<CharacterDTO> {
    return this.httpService.get<CharacterDTO>(this.api);
  }

  getMoreCharacters(page: number): Observable<CharacterDTO> {
    return this.httpService.get<CharacterDTO>(`${this.api}?page=${page}`);
  }

  getFilteredCharacter(name: string, status: string): Observable<CharacterDTO> {
    return this.httpService.get<CharacterDTO>(
      `${this.api}/?name=${name}&status=${status}`
    );
  }

  getASingleCharacter(id: number): Observable<Result> {
    return this.httpService.get<Result>(`${this.api}/${id}`);
  }
}
