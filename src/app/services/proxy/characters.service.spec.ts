import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { CharacterProxyService } from './character-proxy.service';
import { CharactersService } from './characters.service';

describe('CharactersService', () => {
  let service: CharactersService;
  let proxy: CharacterProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
    service = TestBed.inject(CharactersService);
    proxy = TestBed.inject(CharacterProxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should bring 20 characters', () => {
    const spyOn = jest
      .spyOn(proxy, 'getCharacters')
      .mockImplementation(() => of);
  });
});
