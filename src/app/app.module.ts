import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AllCharactersComponent } from './layout/all-characters/all-characters.component';
import { HomeComponent } from './layout/home/home.component';
import { LayoutModule } from './layout/layout.module';
import { RandomCharacterComponent } from './layout/random-character/random-character.component';
import { ProxyModule } from './services/proxy/proxy.module';

const ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'allCharacters', component: AllCharactersComponent },
  { path: 'randomCharacter', component: RandomCharacterComponent },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  declarations: [AppComponent, NavbarComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    ProxyModule,
    LayoutModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
